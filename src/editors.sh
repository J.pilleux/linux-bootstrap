#!/bin/sh

# User editors

install_zsh() {
    msg "Installing oh-my-zsh and required modules"
    mkdir -p ${HOME}/.config/zsh
    echo -e "n\n" | sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    msg 'Installing ZSH additional plugins.'
    git clone https://github.com/agkozak/zsh-z $ZSH_CUSTOM/plugins/zsh-z
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM}/plugins/zsh-syntax-highlighting
}

install_doom_emacs() {
    test -d ${HOME}/.doom.d && msg_warn 'Doom emacs already installed' && return 0
    git clone --depth 1 https://github.com/hlissner/doom-emacs ${HOME}/.emacs.d &&
    ${HOME}/.emacs.d/bin/doom install
}

install_vim_plug() {
    sh -c 'curl -fLo "${XDG_CONFIG_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
}

install_space_vim() {
    test -d ${HOME}/.SpaceVim && msg_warn 'SpaceVim already installed' && return 0
    curl -sLf https://spacevim.org/install.sh | bash
}

sync_doom_emacs() {
    test -f ${HOME}/.emacs.d/bin/doom && (
        msg "Syncing Doom Emacs."
        ${HOME}/.emacs/bin/doom sync
    )
}

install_last_nvim_for_debian() {
    # This requires wget and rsync and sudo privileges
    local nvim_url="https://github.com/neovim/neovim/releases/download/nightly/nvim-linux64.tar.gz"
    sudo apt intsall wget rsync
    cd ${HOME}
    wget ${nvim_url}
    tar xzf nvim-linux64.tar.gz
    cd nvim-linux64
    sudo cp bin/nvim /usr/bin/nvim
    sudo rsync -a lib/* /usr/lib/
    sudo rsync -a share/* /usr/share/
}

#!/bin/sh

# Echo colors codes
YELLOW='\033[33m'
GREEN='\033[32m'
RED='\033[31m'
COLOR_OFF='\033[0m'

# Display functions

msg() {
    echo -e '--' ${@};
}

msg_warn() {
    msg ${YELLOW}'WARNING'${COLOR_OFF} ':' ${@}
}

msg_success() {
    msg ${GREEN}'SUCCESS'${COLOR_OFF} ':' ${@}
}

msg_error() {
    msg ${RED}'ERROR'${COLOR_OFF} ':' ${@}
}

ask_user() {
    local question=${1}
    echo -n ${question}
    read choice
    case ${choice} in
        y|Y|'' ) echo && return 0 ;;
        n|N    ) echo && return 1 ;;
        *      ) msg_error 'Please answer y or n' ;;
    esac
}


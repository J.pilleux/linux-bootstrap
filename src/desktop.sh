#!/bin/sh

# Desktop environments

cloning_config() {
    msg "Cloning configuration."
    DOTFILES=${HOME}/.cfg
    BACKUP=${HOME}/.cfg-backup
    REPO=https://gitlab.com/j.pilleux/dotfiles.git
    test -d "${DOTFILES}" && (
        msg_warn "${DOTFILES} folder already exists"
        return 1;
    )
    git clone --bare ${REPO} ${DOTFILES} &&
    git --git-dir=${DOTFILES} --work-tree=${HOME} config status.showUntrackedFiles no &&
    mkdir -p $BACKUP &&
    git --git-dir=${DOTFILES} --work-tree=${HOME} checkout 2>&1 | \
    egrep "\s+\." | awk {'print $1'} | xargs -I{} bash -c 'mv -f {} $HOME/.cfg-backup/"$(basename {})"' &&
    git --git-dir=${DOTFILES} --work-tree=${HOME} checkout && return 0 || return 1
}

install_dwm() {
    install_dmenu
    compile_dwm
    add_to_desktops
}

install_dmenu() {
    local dmenu_path=${HOME}/.config/dmenu
    test -d ${dmenu_path} && (
        rm -f ${dmenu_path}/config.h
        sudo make -C ${dmenu_path} clean install
    )
}

compile_dwm() {
    msg 'Compiling DWM'
    local src_path=${HOME}/.config/dwm
    test -d ${src_path} && (
        rm -f ${dmenu_path}/config.h
        sudo make -C ${src_path} clean install
    )
}

add_to_desktops() {
    msg 'Adding DWM to desktop sessions'
    dwm_path=/usr/share/xsessions/dwm.desktop
    start_path=/usr/share/xsessions/dwm_start.sh
    sudo ln -s ${HOME}/.config/dwm/start.sh ${start_path}
    test ! -f ${dwm_path} && (
        sudo echo '[Destop Entry]' > ${dwm_path}
        sudo echo 'Name=DWM' >> ${dwm_path}
        sudo echo 'Comment=Log into the DWM session' >> ${dwm_path}
        sudo echo 'Exec=$start_path' >> ${dwm_path}
        sudo echo 'Type=Application' >> ${dwm_path}
        sudo echo 'Keywords=wm;tiling' >> ${dwm_path}
    ) || msg_warn 'dwm session already exists.'
}


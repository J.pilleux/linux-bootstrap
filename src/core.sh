#!/bin/sh

# Core installation functions

PROGS='tmux git zsh fish vim neovim curl neofetch emacs python3 feh'
PACMAN_PROGS='picom python-pip'
APT_PROGS='compton python3-pip'

install_programs() {
    msg "Installing basic programs."
    command -v apt &> /dev/null && sudo apt install -y ${PROGS} ${APT_PROGS} && return 0
    command -v pacman &> /dev/null && sudo pacman -S ${PROGS} ${PACMAN_PROGS} && return 0
}

install_graphical() {
    command -v pacman &> /dev/null || return 0
    ask_user 'Do you want to install xorg and lightdm ? [Y/n]' &&
        pacman -S xorg lightdm
    ask_user 'Do you want to install nvidia graphics ? [Y/n]' &&
        pacman -S nvidia
}

install_yay() {
    command -v pacman &> /dev/null || return 0
    pacman -S --needed 'git base-devel' # Making sure we have the correct dependencies
    git clone https://aur.archlinux.org/yay.git /tmp/yay &&
    cd /tmp/yay &&
    makepkg -si
    cd ${HOME}
}

get_wallpapers() {
    msg 'Getting wallpapers'
    local wallpaper_path=$HOME/.local/wallpapers
    test ! -d ${wallpaper_path} && (
        mkdir -p ${wallpaper_path}
        git clone https://gitlab.com/j.pilleux/wallpapers.git ${wallpaper_path}
  )
}

change_shell() {
    msg "Please choose a shell to use :"
    usershell=
    select opt in Bash Zsh Fish "Keep actual"
    do
        case ${opt} in
            "Bash" ) usershell=/bin/bash ; break ;;
            "Zsh" ) usershell=/bin/zsh ; break ;;
            "Fish" ) usershell=/usr/bin/fish ; break ;;
            "Keep actual" ) break ;;
        esac
    done
    test ! -z ${usershell} && (
        sudo usermod -s ${usershell} ${USER}
        msg "${USER} shell changed to ${opt}"
    ) ||
    msg "${USER} shell unchanged"
}


#!/bin/sh

. messaging.sh

install_packages() {
    local prog_list=${1};
    if command -v apt &> /dev/null; then
        msg "Debian base distro detected, installing ${prog_list}"
        sudo apt install -y ${prog_list} > /dev/null
    elif command -v pacman &> /dev/null; then
        msg "Arch base distro detected, installing ${prog_list}"
        sudo pacman -S ${prog_list} > /dev/null
    fi
    test "${?}" == "0" &&
        msg_success "Packages ${prog_list} installed" ||
        msg_error "Error during installation"
}


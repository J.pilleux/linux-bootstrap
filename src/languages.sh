#!/bin/sh

. systools.sh

install_haskell_deps() {
    stack install ghc-mod hlint hdevtools hfmt
}

install_haskell_deb() {
    install_packages 'haskell-platform'
    curl -sSL https://get.haskellstack.org/ | sh
    install_haskell_deps
}

install_haskell_arch() {
    install_packages 'ghc cabal-install stack'
    install_haskell_deps
}


#!/bin/sh

# Sourcing functions
SRC_FOLDER=./src

. ${SRC_FOLDER}/messaging.sh
. ${SRC_FOLDER}/core.sh
. ${SRC_FOLDER}/editors.sh
. ${SRC_FOLDER}/desktop.sh

# Force the script to be executed from home
cd ${HOME};

msg "Exporting env variables for current script."
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_CONFIG_HOME="${HOME}/.config"
export ZDOTDIR="${HOME}/.config/zsh"
export ZSH="${HOME}/.config/oh-my-zsh"
export HISTFILE="${MY_CONFDIR}/zsh/.zsh_history"
export ZSH_CUSTOM="${ZSH}/custom"

# Installation calls
add_env_vars

install_programs

install_graphical

ask_user 'Do you want to install yay (AUR) ? [Y/n]' && install_yay

get_wallpapers

ask_user 'Do you want to install oh-my-zsh ? [Y/n]' && install_zsh

change_shell

ask_user 'Do you want to install Doom Emacs ? [Y/n]' && install_doom_emacs

ask_user 'Do you want to install Space Vim ? [Y/n]' && install_space_vim

install_vim_plug

cloning_config

sync_doom_emacs

ask_user 'Do you want to install DWM ? [Y/n]' && install_dwm

msg_success "Everything's done !"

exit 0;

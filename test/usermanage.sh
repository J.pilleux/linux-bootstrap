#!/bin/sh
set -euo pipefail

username=test
installfile=/home/${username}/install.sh
gl_repo=https://gitlab.com/J.pilleux/linux-bootstrap/-/raw/master/config.sh
usershell=/bin/bash

userdel -rf ${username} || echo 'User does not exists'
useradd -g sudo -m ${username}
echo -e 'test\ntest' | passwd ${username}
chsh -s ${usershell} ${username}

test -d /home/${username} && (
    curl -sLf ${gl_repo} > ${installfile}
    chown ${username} ${installfile}
)
